module Gluttonberg
  module Tv
    class Engine < ::Rails::Engine
      config.widget_factory_name = "Gluttonberg TV"

      initializer "extend assets model functionality" do |app|
        Gluttonberg::MixinManager.register_mixin("Gluttonberg::Asset", Tv::VideoMixin)
        config.asset_processors += [Tv::VideoProcessor]
      end

      rake_tasks do
        load File.join(File.dirname(__FILE__), 'tasks/tv_tasks.rake')
      end

    end
  end
end