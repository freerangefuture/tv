# desc "Explaining what the task does"
# task :tv do
#   # Task goes here
# end

require 'rubygems'

namespace :gluttonberg do
  namespace :tv do

    desc "initlize default video settings"
    task :init_video_settings => :environment do
      Gluttonberg::Tv::VideoSetting.init
    end

    desc "copy all videos to S3 and then invalidate CloudFront"
    task :copy_videos_to_s3 => :environment do
      category = Gluttonberg::AssetCategory.where(:name => "video").first
      unless category.blank?
        category.assets.each do |video|
          video.copy_videos_to_s3
        end
        Gluttonberg::Asset.invlidate_all_videos
      end
    end

    desc "Invalidate all videos on CloudFront"
    task :invalidate_videos_on_cloudfront => :environment do
      Gluttonberg::Asset.invlidate_all_videos
    end

    desc "regenerate all video assets"
    task :regenerate_video_assets => :environment do
      category = Gluttonberg::AssetCategory.where(:name => "video").first
      unless category.blank?
        assets = category.assets
        assets.each do |asset|
          if !File.exist?(asset.tmp_location_on_disk) && !File.exist?(asset.tmp_original_file_on_disk)
            asset.download_asset_to_tmp_file
          end
          Gluttonberg::Tv::VideoProcessor.process(asset)
        end
      end
      puts "Completed!"
    end
  end
end