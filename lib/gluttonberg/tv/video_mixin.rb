module Gluttonberg
  module Tv
    module VideoMixin
      extend ActiveSupport::Concern

      class S3Storage
        include Gluttonberg::Library::Storage::S3
      end
      module ClassMethods
        def invlidate_all_videos
          key_id = Gluttonberg::Setting.get_setting("s3_key_id")
          key_val = Gluttonberg::Setting.get_setting("s3_access_key")

          if !key_id.blank? && !key_val.blank?
            acf = AWS::CloudFront::Client.new({
              :access_key_id => key_id,
              :secret_access_key => key_val
            })
            unless acf.blank?
              list = acf.list_distributions
              if !list.blank? && !list.items.blank?
                distibution = list.items.first
                unless distibution.blank?
                  videos = Gluttonberg::AssetCategory.where(:name => "video").first.assets
                  fti = []
                  videos.each do |v|
                    Gluttonberg::Tv::VideoSetting.all.each do |setting|
                      local_file = Pathname.new("#{v.tmp_directory}/#{v.filename_without_extension}_#{setting.file_postfix}")
                      base_name = File.basename(local_file)
                      folder = v.asset_hash
                      fti << "/user_assets/" + folder + "/" + base_name
                    end
                  end
                  files_array = fti.each_slice(900).to_a

                  files_array.each do |files|
                    puts "Invalidating #{files.size} file/s"
                    acf.create_invalidation({
                      :distribution_id => distibution[:id],
                      :invalidation_batch => {
                        :paths =>  {
                          :items => files ,
                          :quantity => files.length
                        },
                        :caller_reference => "gluttonberg_tv_videos_invalidate_batch_#{Time.now.to_i}"
                      }
                    })
                  end
                end
              end # distribution
            end #acf
          end #key

        end
      end #ClassMethods

      #InstanceMethods
      def url_for_processed_video
        "/user_assets/#{asset_hash}/processed_#{self.filename_without_extension}.mp4"
      end

      def video_url_for_postfix(pf)
        "/user_assets/#{asset_hash}/#{self.filename_without_extension}_#{pf}"
      end

      def copy_videos_to_s3
        folder = self.tmp_directory
        #transfer original file
        S3Storage.migrate_file_to_s3(folder , self.file_name) if Rails.configuration.asset_storage == :s3
        # transfer procesed files
        Gluttonberg::Tv::VideoSetting.all.each do |setting|
          begin
            local_file = Pathname.new("#{self.tmp_directory}/#{self.filename_without_extension}_#{setting.file_postfix}")
            base_name = File.basename(local_file)
            mime_type = ''
            if setting.file_postfix.include?("mp4")
              mime_type = 'video/mp4'
            elsif setting.file_postfix.include?("webm")
              mime_type = 'video/webm'
            elsif setting.file_postfix.include?("ogv")
              mime_type = 'video/ogg'
            end
            if Rails.configuration.asset_storage == :s3
              S3Storage.migrate_file_to_s3(self.asset_hash , base_name , mime_type)
              self.update_attributes(:copied_to_s3 => true)
              puts "Copied #{base_name}"
            end
            remove_file_from_tmp_storage
          rescue => e
            puts "#{base_name} failed to copy"
            puts "** #{e} **"
          end
        end #filetypes
      end
    end
  end
end