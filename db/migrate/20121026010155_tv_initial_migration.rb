class TvInitialMigration < ActiveRecord::Migration
  def up
    create_table :tv_video_settings do |t|
      t.string :name
      t.string :file_postfix
      t.text :command
      t.timestamps
    end

  end

  def down
    drop_table :tv_video_settings
  end
end
