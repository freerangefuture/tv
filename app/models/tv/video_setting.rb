module Tv
  class VideoSetting < ActiveRecord::Base
    self.table_name = "tv_video_settings"
    attr_accessible :name, :file_postfix, :command

    #initialize with default video settings
    def self.init
      self.create(:name => "High MP4", :file_postfix => "high.mp4" , :command => "ffmpeg -i $file$ -vb 1500k -vcodec libx264 -g 30 -y")
      self.create(:name => "High WEBM", :file_postfix => "high.webm" , :command => "ffmpeg -i $file$ -vb 1500k -vcodec libvpx -acodec libvorbis -ab 160000 -f webm -g 30 -y")
      self.create(:name => "High OGV", :file_postfix => "high.ogv" , :command => "ffmpeg -i $file$ -vb 1500k -vcodec libtheora -acodec libvorbis -ab 160000 -g 30 -y")
      self.create(:name => "Medium MP4", :file_postfix => "medium.mp4" , :command => "ffmpeg -i $file$ -vb 600k -vcodec libx264 -g 30 -y")
      self.create(:name => "Medium WEBM", :file_postfix => "medium.webm" , :command => "ffmpeg -i $file$ -vb 600k -vcodec libvpx -acodec libvorbis -ab 160000 -f webm -g 30 -y")
      self.create(:name => "Medium OGV", :file_postfix => "medium.ogv" , :command => "ffmpeg -i $file$ -vb 600k -vcodec libtheora -acodec libvorbis -ab 160000 -g 30 -y")
      self.create(:name => "Low MP4", :file_postfix => "low.mp4" , :command => "ffmpeg -i $file$ -vb 300k -vcodec libx264 -g 30 -y")
      self.create(:name => "Low OGV", :file_postfix => "low.ogv" , :command => "ffmpeg -i $file$ -vb 300k -vcodec libtheora -acodec libvorbis -ab 160000 -g 30 -y")
      self.create(:name => "Low WEBM", :file_postfix => "low.webm" , :command => "ffmpeg -i $file$ -vb 300k -vcodec libvpx -acodec libvorbis -ab 160000 -f webm -g 30 -y")
    end

  end
end