class VideoJob
  include Sidekiq::Worker

  def perform(video)
    asset = Gluttonberg::Asset.find(video)
    commands = []
    paths = []
    if Gluttonberg::Setting.get_setting("video_assets") == "Enable"
      ffmpeg_gb_setting = Gluttonberg::Tv::VideoSetting.all
      unless ffmpeg_gb_setting.blank?
        ffmpeg_gb_setting.each_with_index do |s, i|
          paths << "#{save_asset_to(asset)}/#{asset.filename_without_extension}_#{s.file_postfix}"
          commands << "#{s.command} #{paths[i]}"
        end
        if File.exist?(asset.tmp_location_on_disk) || File.exist?(asset.tmp_original_file_on_disk)
          actual_file = File.exist?(asset.tmp_location_on_disk) ? asset.tmp_location_on_disk : asset.tmp_original_file_on_disk
          options = {:file => actual_file}
          commands.each_with_index do |command, i|
            begin
              puts "========== Starting to encode #{command} =========="
              puts "========== Path: #{actual_file} =========="
              transcoder = RVideo::Transcoder.new
              result = transcoder.execute(command, options)
              asset.processed = true
              asset.save
              system("qtfaststart #{paths[i]}")
            rescue => e
              asset.processed = true
              asset.save
              puts "========== Unable to transcode file: #{e.class} - #{e.message} =========="
              system("qtfaststart #{paths[i]}")
            end
          end
        end
        if !Gluttonberg::Setting.get_setting("s3_key_id").blank? && !Gluttonberg::Setting.get_setting("s3_access_key").blank? && !Gluttonberg::Setting.get_setting("s3_server_url").blank? && !Gluttonberg::Setting.get_setting("s3_bucket").blank?
          asset.copy_videos_to_s3
        end
      end #empty settings
    end #setting enabled
  end

  def save_asset_to(asset)
    asset.tmp_directory
  end

end
