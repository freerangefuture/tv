$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "gluttonberg/tv/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "gluttonberg-tv"
  s.version     = Gluttonberg::Tv::VERSION
  s.authors     = ["Nick Crother", "Abdul Rauf", "Yuri Tomanek"]
  s.email       = ["office@freerangefuture.com"]
  s.homepage    = "http://gluttonberg.com"
  s.summary     = "Gluttonberg TV is a set of technologies for the Gluttonberg CMS that enable the simple management of high quality HTML5 video within your website."
  s.description = "Gluttonberg TV is a set of technologies for the Gluttonberg CMS that enable the simple management of high quality HTML5 video within your website. One upload converts your video to all required HTML5 formats, transfers the videos to the cloud and pushes them out to a distributed content delivery network so that the videos load super fast for your audience, wherever they are in the world."

  s.files = Dir["{app,config,db,lib}/**/*"] + ["Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 3.2.17"
  s.add_dependency "gluttonberg-core", "~> 3.0"

end
