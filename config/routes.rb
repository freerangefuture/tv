#Tv::Engine.routes.draw do
Rails.application.routes.draw do
  scope :module => 'gluttonberg' do
    scope :module => 'tv' do
      namespace :admin do
        scope :module => 'settings' do
          resources :video_settings do
            get 'delete', :on => :member
          end
        end
      end
    end
  end
end
